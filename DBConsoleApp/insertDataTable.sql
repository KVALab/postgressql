
INSERT INTO currencies (currency_name, description)
VALUES
('RUB', 'Рубль'),
('USD', 'Доллар'),
('EUR', 'Евро'),
('INR', 'Руппи'),
('JPY', 'Йен');


INSERT INTO users (first_name, last_name, login, password)
VALUES ('Пользователь 001', 'Фамилия 001', 'login001', 'gjrbJGHe01'),
('Пользователь 002', 'Фамилия 002', 'login002', 'gjrbJGHe02'),
('Пользователь 003', 'Фамилия 003', 'login003', 'gjrbJGHbg'),
('Пользователь 004', 'Фамилия 004', 'login004', 'gjrbJGHefd'),
('Пользователь 005', 'Фамилия 005', 'login005', 'gjrbJG45f');

INSERT INTO accounts (ac_name, amount, description, creation_date, fk_currency_id, fk_user_id )
VALUES 
('Account01', 5000, 'Описание 01', '2010-09-10', 1, 1),
('Account02', 10000, 'Описание 02', '2011-12-10', 2, 1),
('Account03', 3000, 'Описание 03', '2012-08-10', 1, 4),
('Account04', 4000, 'Описание 04', '2009-09-10', 3, 5),
('Account05', 15000, 'Описание 05', '2011-01-10', 2, 3);
