﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace DBConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationContex db = new())
            {
                var currencies = db.Curencies.ToList();
                Console.WriteLine("Список валют:");
                foreach (var c in currencies)
                    Console.WriteLine($"| {c.CurrencyName,6} | {c.Description,10} |");
                Console.WriteLine();


                Console.WriteLine("Список пользователей:");
                var users = db.Users.ToList();
                foreach (var u in users)
                    Console.WriteLine($"| {u.FirstName,15} | {u.LastName,10} | {u.Login,10} |");
                Console.WriteLine();


                Console.WriteLine("Список аккаунтов:");
                var accounts = db.Accounts.ToList();
                foreach (var a in accounts)
                    Console.WriteLine($"| {a.AccountName,10} | {a.CreationDate} | {a.Amount,10} | {a.Description} |");
                Console.WriteLine();


                Console.WriteLine("Пользователи с аккаунтами:");
                foreach (var u in db.Users.Include(i => i.Accounts).ToList())
                {
                    Console.WriteLine($"{u.FirstName} {u.LastName} :");
                    foreach (var a in u.Accounts)
                    {
                        Console.WriteLine($"\t{a.AccountName,12} {a.CreationDate} {a.Amount,8} {a.Currency?.CurrencyName}");
                    }
                }
            }

            Console.WriteLine("\nДобавьте нового пользователя.");
            Console.Write("Введите имя: ");
            string firstName = Console.ReadLine();
            Console.Write("Введите фамилию: ");
            string lastName = Console.ReadLine();
            Console.Write("Введите логин: ");
            string login = Console.ReadLine();
            Console.Write("Введите пароль: ");
            string password = Console.ReadLine();

            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) ||
                string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                Console.WriteLine("Введены не корректные данные для добавления пользователя!");
            }
            else
            {
                using (ApplicationContex db = new())
                {
                    try
                    {
                        User user = new User()
                        {
                            FirstName = firstName,
                            LastName = lastName,
                            Login = login,
                            Password = password
                        };
                        db.Users.Add(user);
                        db.SaveChanges();
                    }
                    catch (Npgsql.NpgsqlException e)
                    {
                        Console.WriteLine("Не удалось добавить пользователя в БД!\n" + e.Message);
                    }

                    Console.WriteLine("\nСписок пользователей после добавления нового:");
                    var users = db.Users.ToList();
                    foreach (var u in users)
                        Console.WriteLine($"| {u.FirstName,15} | {u.LastName,10} | {u.Login,10} |");
                    Console.WriteLine();
                }
            }
        }
    }
}
