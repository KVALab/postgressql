﻿namespace DBConsoleApp
{
    public class Currency
    {        
        public int Id { get; set; }
        public string CurrencyName { get; set; }
        public string Description { get; set; }
    }
}