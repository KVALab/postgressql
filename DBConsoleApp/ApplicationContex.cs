﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace DBConsoleApp
{
    public class ApplicationContex : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Currency> Curencies { get; set; }
        public DbSet<Account> Accounts { get; set; }

        private readonly StreamWriter logf = new StreamWriter("logQuery.txt", true);

        public ApplicationContex()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appconfig.json", optional: false, reloadOnChange: true).Build();
            var connectionString = configuration.GetConnectionString("defaultConnection");

            optionsBuilder.UseNpgsql(connectionString);

            optionsBuilder.LogTo(logf.WriteLine, new[] { RelationalEventId.CommandExecuted });
        }

        public override void Dispose()
        {
            base.Dispose();
            logf.Dispose();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");
                entity.HasKey(k => k.Id);
                entity.Property(p => p.Id)
                    .HasColumnName("id");
                entity.Property(p => p.FirstName)
                    .HasColumnName("first_name")
                    .IsRequired();
                entity.Property(p => p.LastName)
                    .HasColumnName("last_name")
                    .IsRequired();
                entity.Property(p => p.Login)
                    .HasColumnName("login")
                    .IsRequired();
                entity.Property(p => p.Password)
                    .HasColumnName("password")
                    .IsRequired();
                entity.HasAlternateKey(k => k.Login);

            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.ToTable("currencies");
                entity.HasKey(k => k.Id);
                entity.Property(p => p.Id)
                        .HasColumnName("id");
                entity.Property(p => p.CurrencyName)
                        .HasColumnName("currency_name")
                        .HasMaxLength(3)
                        .IsRequired();
                entity.Property(p => p.Description)
                        .HasColumnName("description");
            });

            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("accounts");
                entity.HasKey(k => k.Id);
                entity.Property(p => p.Id)
                     .HasColumnName("id");
                entity.Property(p => p.AccountName)
                     .HasColumnName("ac_name")
                     .IsRequired();
                entity.Property(p => p.Amount)
                     .HasColumnName("amount");
                entity.Property(p => p.CreationDate)
                     .HasColumnType("timestamp with time zone")
                     .HasColumnName("creation_date");
                entity.Property(p => p.Description)
                     .HasColumnName("description")
                     .HasMaxLength(100);

                entity.Property(p => p.UserId).HasColumnName("fk_user_id");
                entity.Property(p => p.CurrencyId).HasColumnName("fk_currency_id");

                entity.HasOne(d => d.User)
                     .WithMany(m => m.Accounts)
                     .HasForeignKey(k => k.UserId)
                     .HasConstraintName("accounts_fk_user_id_fkey");

                entity.HasOne(a => a.Currency);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
