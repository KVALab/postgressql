﻿using System.Collections.Generic;

namespace DBConsoleApp
{
    public class User
    {
        public User()
        {
            Accounts = new HashSet<Account>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
