﻿
using System;

namespace DBConsoleApp
{
    public class Account
    {
        public int Id { get; set; }
        public string AccountName { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }

        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
